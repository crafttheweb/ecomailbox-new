<div class="row">
    <div class="col-md-12">
        <ul class="dealsList">
          <?php
              while ( $loop->have_posts() ) : $loop->the_post();
              $start_date = date('d/m/Y', strtotime(get_post_meta($post->ID, "circular_start_date", true)));
              $end_date = date('d/m/Y', strtotime(get_post_meta($post->ID, "circular_end_date", true)));
          ?>
            <li>
              <a href="#" data-toggle="modal" data-target="#testModal">
                  <div class="listImage">
                      <div class="hover">
                          <p><?php the_title(); ?></p>
                      </div>
                      <?php the_field("circular_description") ?>
                  </div>
                  <div class="logo">
                      <img src="images/logo/LogoImage_5931.jpg">
                  </div>
                  <div class="details">
                      <div class="startDate text-left">
                          <span>Start Date</span><br>
                          <span class="date"><?php echo $start_date; ?></span>
                      </div>
                      <div class="endDate text-right">
                          <span>End Date</span><br>
                          <span class="date"><?php echo $end_date; ?></span>
                      </div>
                  </div>
              </a>
          </li>
        <?php endwhile; ?>
      </ul>
  </div>
</div>
