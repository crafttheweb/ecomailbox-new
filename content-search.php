<?php
$start_date = date('d/m/Y', strtotime(get_post_meta($post->ID, "circular_start_date", true)));
$end_date = date('d/m/Y', strtotime(get_post_meta($post->ID, "circular_end_date", true)));
 ?>
<li>
  <a href="<?php echo esc_url( get_permalink($post->ID) ); ?>">
      <div class="logo">
          <?php
          $author = get_the_author_meta('ID');
          $url = get_field('logo', 'user_'. $author );
          ?>
          <img src="<?php echo $url; ?>">
      </div>

      <div class="listImage">
          <div class="hover">
              <p><?php the_title(); ?></p>
          </div>
          <?php
            remove_filter ('acf_the_content', 'wpautop');
            the_field('circular_description');
          ?>
      </div>

      <div class="details">
          <div class="startDate text-left">
              <span>Start Date</span><br>
              <span class="date"><?php echo $start_date; ?></span>
          </div>
          <div class="endDate text-right">
              <span>End Date</span><br>
              <span class="date"><?php echo $end_date; ?></span>
          </div>
      </div>
  </a>
</li>



