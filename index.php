<?php get_header();

  $args = array('post_type' => 'Circular', 'posts_per_page' => 10, 'meta_key'=>'position', 'orderby' => 'meta_value_num', 'order' => 'ASC',
  'tax_query' => array(
      array(
        'taxonomy' => 'home_page_section',
        'field'    => 'slug',
        'terms'    => array('todays-deals'),
      ),
    )
  );
  $loop = new WP_Query($args);
  if ( $loop->have_posts() ) :
?>
<section id="todaysDeal">
    <div class="container">
      <div class="row">
          <div class="col-md-12">
              <div class="title">
                  <h3>Todays <span>Deals</span></h3>
              </div>
          </div>
      </div>
      <div class="row">
          <div class="col-md-12">
              <ul class="dealsList">
                <?php
                  	while ( $loop->have_posts() ) : $loop->the_post();
                ?>
                <?php get_template_part( 'content' );  ?>
              <?php endwhile; ?>
            </ul>
        </div>
      </div>


  		<div class="row">
  			<div class="col-md-12">
  				<div class="viewAll grey">
  					<div class="viewAllLine"></div>
  					<a href="<?php echo esc_url( home_url( '/view-all?viewAll=todays-deals' )); ?>">View All</a>
  				</div>
  			</div>
  		</div>
		</div>
</section>
<?php endif; ?>
<?php wp_reset_query(); ?>

<section id="weeksMailer" class="bgGray">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title">
                    <h3>This Week's <span>Mailers</span></h3>
                </div>
            </div>
        </div>
	      <?php
          $args = array('post_type' => 'Circular', 'posts_per_page' => 5, 'meta_key'=>'position', 'orderby' => 'meta_value_num', 'order' => 'ASC',
          'tax_query' => array(
          		array(
          			'taxonomy' => 'home_page_section',
          			'field'    => 'slug',
          			'terms'    => array('this-weeks-mailers'),
          		),
          	)
          );
          $loop = new WP_Query($args);
          if ( $loop->have_posts() ) :
        ?>

        <div class="row">
            <div class="col-md-12">
                <ul class="dealsList">
                    <?php
                    while ( $loop->have_posts() ) : $loop->the_post();
                    ?>
                    <?php get_template_part( 'content' );  ?>
                    <?php endwhile; ?>

                </ul>
            </div>
        </div>
        <?php endif; ?>
        <?php wp_reset_query(); ?>
        <div class="row">
            <div class="col-md-12">
                <div class="viewAll">
                    <div class="viewAllLine"></div>
                    <a href="<?php echo esc_url( home_url( '/view-all?viewAll=this-weeks-mailers' )); ?>">View All</a>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="nzMadeProducts">
     <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title">
                        <h3>NZ Made <span>Products</span></h3>
                    </div>
                </div>
            </div>

			<?php
            $args = array('post_type' => 'Circular', 'posts_per_page' => 5, 'meta_key'=>'position', 'orderby' => 'meta_value_num', 'order' => 'ASC',
            'tax_query' => array(
            		array(
            			'taxonomy' => 'home_page_section',
            			'field'    => 'slug',
            			'terms'    => array('nz-made-products'),
            		),
            	)
            );
            $loop = new WP_Query($args);
            if ( $loop->have_posts() ) :
          ?>
          <div class="row">
              <div class="col-md-12">
                  <ul class="dealsList">
                      <?php
                      while ( $loop->have_posts() ) : $loop->the_post();
                      ?>
                      <?php get_template_part( 'content' );  ?>
                      <?php endwhile; ?>

                  </ul>
              </div>
          </div>
			    <?php endif; ?>
          <?php wp_reset_query(); ?>

            <div class="row">
                <div class="col-md-12">
                    <div class="viewAll grey">
                        <div class="viewAllLine">
                        </div>
                        <a href="<?php echo esc_url( home_url( '/product-category/nz-made' )); ?>">View All</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

<section id="moreDeals" class="bgGray">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title">
                        <h3>More <span>Deals</span></h3>
                    </div>
                </div>
            </div>

			<?php
            $args = array('post_type' => 'Circular', 'posts_per_page' => 5, 'meta_key'=>'position', 'orderby' => 'meta_value_num', 'order' => 'ASC',
            'tax_query' => array(
            		array(
            			'taxonomy' => 'home_page_section',
            			'field'    => 'slug',
            			'terms'    => array('more-deals'),
            		),
            	)
            );
            $loop = new WP_Query($args);
            if ( $loop->have_posts() ) :
          ?>

          <div class="row">
              <div class="col-md-12">
                  <ul class="dealsList">
                      <?php
                      while ( $loop->have_posts() ) : $loop->the_post();
                      ?>
                      <?php get_template_part( 'content' );  ?>
                      <?php endwhile; ?>

                  </ul>
              </div>
          </div>
			     <?php endif; ?>
            <?php wp_reset_query(); ?>


            <div class="row">
                <div class="col-md-12">
                    <div class="viewAll grey">
                        <div class="viewAllLine">
                        </div>
                        <a href="<?php echo esc_url( home_url( '/view-all?viewAll=more-deals' )); ?>">View All</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

<section id="blog">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="title">
          <h3>Our <span>Blogs</span></h3>
        </div>
      </div>
    </div>

    <?php
    $args = array( 'numberposts' => 3);
    $posts= get_posts( $args );
    if ($posts) :
    ?>
    <div class="row">
      <ul class="blogList">
        <?php get_template_part( 'content-all-blog' );  ?>
      </ul>
    </div>
  <?php endif; ?>

    <div class="row">
      <div class="col-md-12">
        <div class="viewAll grey">
          <div class="viewAllLine"></div>
          <a href="<?php echo esc_url( home_url( '/view-all?viewAll=blog' )); ?>">View All</a>
        </div>
      </div>
    </div>
  </div>
</section>

<?php get_footer(); ?>
