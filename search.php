<?php get_header(); ?>

<?php
$getlocation = $_GET['location'];
$getsearch = $_GET['s'];
  $args = array('post_type' => array('Circular','nz_made_post'),'s' => $getsearch, 'posts_per_page' => -1);
  $loop = new WP_Query($args);

  if ( $loop->have_posts() ) :
?>
<section id="todaysDeal">
    <div class="container">
      <div class="row">
          <div class="col-md-12">
              <div class="title">
                  <h3>Search in <span><?php echo $getlocation; ?></span></h3>
              </div>
          </div>
      </div>
      <div class="row">
          <div class="col-md-12">
              <ul class="dealsList">
                <?php
                  	while ( $loop->have_posts() ) : $loop->the_post();
                ?>
                <?php get_template_part( 'content' );  ?>
				<?php //get_template_part( 'content', 'search' );  ?>
              <?php endwhile; ?>
            </ul>
        </div>
      </div>
		</div>
</section>
<?php else: ?>
<section>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
          <?php echo "<h3>Oops! Sorry we could'nt find any deals in this area.</h3>"; ?>
      </div>
    </div>
  </div>
</section>
<?php endif; ?>
<?php wp_reset_query(); ?>

<?php get_footer(); ?>
