
<footer id="footer">
        <div class="topFooter">
            <div class="container">
              <div class="row">
                <div class="col-md-6">
                  <div class="row">
                      <div class="col-md-12">
                          <h1>Reduce paper waste with Ecomailbox</h1>
                          <h3>GET YOUR FREE NO AD MAIL STICKER</h3>
                      </div>
                      <div class="col-md-12">
                        <a href="<?php echo home_url('/sign-up'); ?>" class="btn btn-default">Click Here</a>
                      </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="row">
                    <div class="col-md-12">
                        <h1>Signup For The Best Weekly Deals</h1>
                        <!-- Begin MailChimp Signup Form -->
                        <!-- <link href="//cdn-images.mailchimp.com/embedcode/slim-10_7.css" rel="stylesheet" type="text/css"> -->
                        <div id="mc_embed_signup">
                        <form action="//facebook.us6.list-manage.com/subscribe/post?u=e1ce49b3a30ec2a2280812a67&amp;id=61e169ea08" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                            <div class="form-group">
                              <input type="email" value="" name="EMAIL" class="email form-control" id="mce-EMAIL" placeholder="email address" required>
                            </div>
                            <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                            <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_e1ce49b3a30ec2a2280812a67_61e169ea08" tabindex="-1" value=""></div>
                            <div class="form-group">
                              <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="btn btn-default">
                            </div>
                        </form>
                        </div>

                        <!--End mc_embed_signup-->
                    </div>

                  </div>
                </div>
              </div>
            </div>
        </div>

        <div class="bottomFooter">
            <div class="container">
                <div class="row">
					<?php

					$location = 'footer2';
					$menu_obj1 = wpse45700_get_menu_by_location($location );

					$args2 = array(

					'menu'	=> 'footer2',
					'theme_location' => 'footer2',

					'menu_class' => 'col-md-2 col-sm-3 col-xs-6',
					'items_wrap'=> '<ul id="%1$s" class="%2$s"><li><span>'.esc_html($menu_obj1->name).'</span></li>%3$s</ul>'
					);

					wp_nav_menu($args2);

					$location = 'footer';
					$menu_obj = wpse45700_get_menu_by_location($location );

					$args = array(

					'menu'	=> 'footer',
					'theme_location' => 'footer',
					'menu_class' => 'col-md-2 col-sm-3 col-xs-6',
					'items_wrap'=> '<ul id="%1$s" class="%2$s"><li><span>'.esc_html($menu_obj->name).'</span></li>%3$s</ul>'
					);

					wp_nav_menu($args);



          $location = 'footer3';
					$menu_obj1 = wpse45700_get_menu_by_location($location );

					$args3 = array(

					'menu'	=> 'footer3',
					'theme_location' => 'footer3',
					'menu_class' => 'col-md-2 col-sm-3 col-xs-6',
					'items_wrap'=> '<ul id="%1$s" class="%2$s"><li><span>'.esc_html($menu_obj1->name).'</span></li>%3$s</ul>'
					);

					wp_nav_menu($args3);

					$location = 'footer4';
					$menu_obj1 = wpse45700_get_menu_by_location($location );

					$args4 = array(

					'menu'	=> 'footer4',
					'theme_location' => 'footer4',
					'menu_class' => 'col-md-2 col-sm-3 col-xs-6',
					'items_wrap'=> '<ul id="%1$s" class="%2$s"><li><span>'.esc_html($menu_obj1->name).'</span></li>%3$s</ul>'
					);


					wp_nav_menu($args4);

					?>

					<ul class="col-md-3 col-sm-6">
						<li></li>
						<li><a href="http://www.wasteminz.org.nz/"><img src="<?php bloginfo('template_directory'); ?>/images/wasteMinz.png" class="img-responsive"></a></li>

					</ul>
                </div>
            </div>
        </div>
    </footer>
    <?php wp_footer(); ?>
</body>
</html>
