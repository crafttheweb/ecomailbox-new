<?php
  function website_resource(){
    wp_enqueue_style('style',get_stylesheet_uri(),'','','screen');
  }
 add_action('wp_enqueue_scripts','website_resource');

  function scripts_with_jquery(){
   // Register the script like this for a theme:

    wp_register_script('jqueryy', get_template_directory_uri() . '/js/jquery-1.11.1.min.js', '','1.1',false);
    wp_register_script('jquery-bxslider-script', get_template_directory_uri() . '/js/jquery.bxslider.js', '','1.1',false);
    wp_register_script('jquery-bootstrap-script', get_template_directory_uri() . '/js/bootstrap.js', '','1.1',false);
    wp_register_script('custom-script', get_template_directory_uri() . '/js/custom.js', '','1.1',false);

    wp_enqueue_script('jqueryy');
    wp_enqueue_script('jquery-bootstrap-script');
    wp_enqueue_script('jquery-bxslider-script');
    wp_enqueue_script('custom-script');
  }
 add_action('wp_enqueue_scripts', 'scripts_with_jquery');
 add_theme_support('menus');
/*******************************************/
/****************INCLUDES*******************/
/*******************************************/

 include('includes/walkermenu.php');
 include('includes/footermenu.php');

/*********************************************************/
/************************ADD ROLES************************/
/*********************************************************/

function addRole(){
  add_role( 'retailer', __('Retailer' ),
    array(
    'read' => true, // true allows this capability
    'edit_posts' => true, // Allows user to edit their own posts
    'edit_pages' => false, // Allows user to edit pages
    'edit_others_posts' => false, // Allows user to edit others posts not just their own
    'create_posts' => true, // Allows user to create new posts
    'manage_categories' => true, // Allows user to manage post categories
    'publish_posts' => true, // Allows the user to publish, otherwise posts stays in draft mode
    'edit_themes' => false, // false denies this capability. User can’t edit your theme
    'install_plugins' => false, // User cant add new plugins
    'update_plugin' => false, // User can’t update any plugins
    'update_core' => false, // user cant perform core updates
    'upload_files' => true
    )
  );

  add_role('company', __( 'Company' ) );
}
add_action('init','addRole');

/******************************************************************************/
/************************** AUTHOR DROPDOWN ON POSTS **************************/
/******************************************************************************/

add_filter('wp_dropdown_users', 'theme_post_author_override');
function theme_post_author_override($output)
{
  // return if this isn't the theme author override dropdown
  global $post, $user_ID;
  if (!preg_match('/post_author_override/', $output)) return $output;

  // return if we've already replaced the list (end recursion)
  if (preg_match ('/post_author_override_replaced/', $output)) return $output;

  // replacement call to wp_dropdown_users
	$output = wp_dropdown_users(array(
	  'echo' => 0,
		'name' => 'post_author_override_replaced',
		'selected' => empty($post->ID) ? $user_ID : $post->post_author,
		'include_selected' => true
	));

	// put the original name back
	$output = preg_replace('/post_author_override_replaced/', 'post_author_override', $output);

  return $output;
}


function change_numberposts( $query ) {
    // Only modify the main query
    if( $query->is_main_query() ){
        // Home page
        if ( $query->is_home() ) {
            $query->set( 'posts_per_page', 6 );
        }
        // Category 21
        //if ( $query->is_category(nz-made-products)) { // parameter for is_category may be Category ID, Category Title, Category Slug or Array of IDs, names, and slugs.
        //    $query->set( 'posts_per_page', 10 );
        //}
    }
}
add_action( 'pre_get_posts', 'change_numberposts' );

/*******************************************************/
/****** Add Thumbnails in Manage Posts/Pages List ******/
/*******************************************************/

if ( !function_exists('AddThumbColumn') && function_exists('add_theme_support') ) {
// for post and page
add_theme_support('post-thumbnails', array( 'post', 'page' ) );
function AddThumbColumn($cols) { $cols['thumbnail'] = __('Thumbnail'); return $cols; }
function AddThumbValue($column_name, $post_id) {
  $width = (int) 60; $height = (int) 60;
if ( 'thumbnail' == $column_name ) {
// thumbnail of WP 2.9
$thumbnail_id = get_post_meta( $post_id, '_thumbnail_id', true );
// image from gallery
$attachments = get_children( array('post_parent' => $post_id, 'post_type' => 'attachment', 'post_mime_type' => 'image') ); if ($thumbnail_id) $thumb = wp_get_attachment_image( $thumbnail_id, array($width, $height), true ); elseif ($attachments) { foreach ( $attachments as $attachment_id => $attachment ) { $thumb = wp_get_attachment_image( $attachment_id, array($width, $height), true ); } } if ( isset($thumb) && $thumb ) { echo $thumb; } else { echo __('None'); } } }
// for posts
add_filter( 'manage_posts_columns', 'AddThumbColumn' ); add_action( 'manage_posts_custom_column', 'AddThumbValue', 10, 2 );
// for pages
add_filter( 'manage_pages_columns', 'AddThumbColumn' ); add_action( 'manage_pages_custom_column', 'AddThumbValue', 10, 2 ); }

/*************************************************************************/
/******************* WORD LIMIT ON HOME PAGE FOR BLOGS *******************/
/*************************************************************************/

add_action('word_limit', 'string_limit_words');
function string_limit_words($string, $word_limit)
{
  $words = explode(' ', $string, ($word_limit + 1));
  if(count($words) > $word_limit)
  array_pop($words);
  return implode(' ', $words);
}

/************************************************************/
/******************** CUSTOM SEARCH FORM ********************/
/************************************************************/

add_theme_support('html5', array('search-form'));

/*************************************************************************/
/********************** GRAVITY FORMS CUSTOMIZATION **********************/
/*************************************************************************/

add_filter( 'gform_enable_password_field', '__return_true' );

add_filter( 'gform_submit_button', 'form_submit_button', 10, 2 );
function form_submit_button( $button, $form ) {
    return "<button type='submit' class='btn btn-primary' id='gform_submit_button_{$form['id']}'><span>Submit</span></button>";
}

add_filter( 'gform_submit_button_7', 'form_submit_button_subscribe', 10, 1 );
function form_submit_button_subscribe( $button ) {
    return "<button type='submit' class='btn btn-default' id='gform_submit_button_7'>Subscribe</button>";
}

add_filter("gform_field_content", "bootstrap_styles_for_gravityforms_fields", 10, 5);
function bootstrap_styles_for_gravityforms_fields($content, $field, $value, $lead_id, $form_id){

    // Currently only applies to most common field types, but could be expanded.
	if($field["type"] != 'hidden' && $field["type"] != 'list' && $field["type"] != 'multiselect' && $field["type"] != 'checkbox' && $field["type"] != 'fileupload' && $field["type"] != 'date' && $field["type"] != 'html' && $field["type"] != 'address') {
        $content = str_replace('class=\'medium', 'class=\'form-control medium', $content);
        $content = str_replace('class=\'large', 'class=\'form-control large', $content);

    }

    if($field["type"] == 'name' || $field["type"] == 'address' || $field["type"] == 'email' || $field["type"] == 'password') {
        $content = str_replace('<input ', '<input class=\'form-control\' ', $content);
    }

    if($field["type"] == 'textarea') {
        $content = str_replace('class=\'textarea', 'class=\'form-control textarea', $content);
    }

    if($field["type"] == 'checkbox') {
        $content = str_replace('li class=\'', 'li class=\'checkbox ', $content);
        $content = str_replace('<input ', '<input style=\'margin-left:1px;\' ', $content);
    }

    if($field["type"] == 'radio') {
        $content = str_replace('li class=\'', 'li class=\'radio ', $content);
        $content = str_replace('<input ', '<input style=\'margin-left:1px;\' ', $content);
    }

 return $content;

}

// End bootstrap_styles_for_gravityforms_fields()

/*****************************************************/
/************ USER PRODUCT CATEGORIES ****************/
/*****************************************************/

function register_user_taxonomy(){

	$labels = array(
		'name' => 'User Category',
		'singular_name' => 'User Category',
		'search_items' => 'Search User Categories',
		'all_items' => 'All User Categories',
		'parent_item' => 'Parent User Category',
		'parent_item_colon' => 'Parent User Category',
		'edit_item' => 'Edit User Category',
		'update_item' => 'Update User Category',
		'add_new_item' => 'Add New User Category',
		'new_item_name' => 'New User Category Name',
		'menu_name' => 'User Category'
	);

	$args = array(
		'hierarchical' => true,
		'labels' => $labels,
		'show_ui' => true,
		'show_admin_column' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => 'user_category')
	);

	register_taxonomy( 'user_category' , 'user' , $args );
}
add_action( 'init', 'register_user_taxonomy' );

function add_user_category_menu() {
    add_submenu_page( 'users.php' , 'User Category', 'User Category' , 'add_users',  'edit-tags.php?taxonomy=user_category' );
}
add_action(  'admin_menu', 'add_user_category_menu' );




add_action( 'show_user_profile', 'show_user_category' );
add_action( 'edit_user_profile', 'show_user_category' );
function show_user_category( $user ) {

    //get the terms that the user is assigned to
    $assigned_terms = wp_get_object_terms( $user->ID, 'user_category' );
    $assigned_term_ids = array();
    foreach( $assigned_terms as $term ) {
        $assigned_term_ids[] = $term->term_id;
    }

    //get all the terms we have
    $user_cats = get_terms( 'user_category', array('hide_empty'=>false) );

    echo "<h3>User Category</h3>";

     //list the terms as checkbox, make sure the assigned terms are checked
    foreach( $user_cats as $cat ) { ?>
        <input type="checkbox" id="user-category-<?php echo $cat->term_id ?>" <?php if(in_array( $cat->term_id, $assigned_term_ids )) echo 'checked=checked';?> name="user_category[]"  value="<?php echo $cat->term_id;?>"/>
        <?php
    	echo '<label for="user-category-'.$cat->term_id.'">'.$cat->name.'</label>';
    	echo '<br />';
    }
}

add_action( 'personal_options_update', 'save_user_category' );
add_action( 'edit_user_profile_update', 'save_user_category' );
function save_user_category( $user_id ) {

	$user_terms = $_POST['user_category'];
	$terms = array_unique( array_map( 'intval', $user_terms ) );
	wp_set_object_terms( $user_id, $terms, 'user_category', false );

	//make sure you clear the term cache
	clean_object_term_cache($user_id, 'user_category');
}


/*****************************************************/
/************ CHECK IF USER IS LOGGED IN *************/
/*****************************************************/
add_action( 'template_redirect', 'redirect_to_specific_page' );

function redirect_to_specific_page() {

	if ( is_page('retailer-login') && is_user_logged_in() ) {
		$currentUser = wp_get_current_user();
		if (in_array( 'administrator', (array) $currentUser->roles ) ) {
			wp_redirect(esc_url( home_url( '/wp-admin' ) ));
		}else {
			wp_redirect(esc_url( home_url( '/add-circulars' ) ));
		  exit;
		}
  }
}


/*****************************************************/
/************ GRAVITY FORM RETAILER LOGIN ************/
/*****************************************************/

add_action( "gform_after_submission_3", "emb_login_form_after_submission", 10, 2 );
function emb_login_form_after_submission($entry, $form) {
	// get the username and pass
	$username = $entry[4];
	$pass = $entry[2];
	$creds = array();
	// create the credentials array
	$creds['user_login'] = $username;
	$creds['user_password'] = $pass;
	// sign in the user and set him as the logged in user
	$sign = wp_signon( $creds );
	wp_set_current_user( $sign->ID );
	if (in_array( 'administrator', (array) $sign->roles )) {
		wp_redirect(esc_url( home_url( '/wp-admin' ) ));
		exit;
	}else {
		wp_redirect(esc_url( home_url( '/add-circulars' ) ));
		exit;
	}
}
/********* GRAVITY FORM RETAILER VALIDATION *********/
add_filter( "gform_field_validation_3", "emb_login_validate_field", 10, 4 );
function emb_login_validate_field($result, $value, $form, $field) {
	// make sure this variable is global
	// this function is fired via recurrence for each field, s

	global $user;
	// validate username
	if ( $field['cssClass'] === 'username' ) {
		$user = get_user_by( 'login', $value );
		if ( empty( $user->user_login ) ) {
			$result["is_valid"] = false;
			$result["message"] = "Invalid username provided.";
		}

	}
	// validate pass
	if ( $field['cssClass'] === 'password' ) {
		if ( !$user or !wp_check_password( $value, $user->data->user_pass, $user->ID ) ) {
			$result["is_valid"] = false;
			$result["message"] = "Invalid password provided.";
		}

	}
	return $result;

}

/********************************************************************/
/************ GRAVITY FORM ADD LOCATIONS IN SIGN UP FORM ************/
/********************************************************************/
add_filter( 'gform_pre_render_7', 'populate_dropdown' );

//Note: when changing drop down values, we also need to use the gform_pre_validation so that the new values are available when validating the field.
add_filter( 'gform_pre_validation_7', 'populate_dropdown' );

//Note: when changing drop down values, we also need to use the gform_admin_pre_render so that the right values are displayed when editing the entry.
add_filter( 'gform_admin_pre_render_7', 'populate_dropdown' );

//Note: this will allow for the labels to be used during the submission process in case values are enabled
add_filter( 'gform_pre_submission_filter_7', 'populate_dropdown' );
function populate_dropdown( $form ) {

    //only populating drop down for form id 5
    if ( $form['id'] != 7 ) {
       return $form;
    }

    //Reading posts for "Business" category;
	$terms = get_terms( "location", array(
					'hide_empty' => 0,
				) );
				$locations = array();
				if ( count($terms) > 0 ):
					foreach ( $terms as $term )
					$locations[] = $term -> name;
				 endif;
    //Creating drop down item array.
    $items = array();

    //Adding initial blank value.
    //$items[] = array( 'text' => '', 'value' => '' );

    //Adding post titles to the items array
    foreach ( $locations as $item ) {
        $items[] = array( 'text' => $item, 'value' => $item );
    }
    //Adding items to field id 8. Replace 8 with your actual field id. You can get the field id by looking at the input name in the markup.
    foreach ( $form['fields'] as &$field ) {
        if ( $field->id == 7 ) {
            $field->choices = $items;
        }
    }

    return $form;
}
/********************************************************************/
/************ GRAVITY FORM DISPLAY IMAGES IN CIRCULAR ***************/
/********************************************************************/
add_filter('gform_notification', 'custom_notification_attachments', 10, 3);
function custom_notification_attachments( $notification, $form, $entry ) {

    if($notification["name"] == "File Attachment Notification"){

        $fileupload_fields = GFCommon::get_fields_by_type($form, array("fileupload"));

        if(!is_array($fileupload_fields))
            return $notification;

        $upload_root = RGFormsModel::get_upload_root();
        foreach($fileupload_fields as $field){
            $url = $entry[$field["id"]];
            $attachment = preg_replace('|^(.*?)/gravity_forms/|', $upload_root, $url);
            if($attachment){
                $notification["attachments"][] = $attachment;
            }
        }

    }

    return $notification;
}

/* search */

function emb_search( $query ) {
		if ( !$query->is_search )
        return $query;

		$getlocation = $_GET['location'];
		   $taxquery = array(
			array(
				'taxonomy' => 'location',
				'field' => 'slug',
				'terms' => array( $getlocation ),
			)
		);

    $query->set( 'tax_query', $taxquery );

	}
add_action( 'pre_get_posts', 'emb_search');

/************IP LOCATION ************/

// function ip_details($IPaddress){
//   $json       = file_get_contents("http://ipinfo.io/{$IPaddress}");
//   $details    = json_decode($json);
//   return $details;
// }









?>
