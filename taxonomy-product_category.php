<?php
$term = get_term_by('slug', get_query_var('product_category'), 'product_category' );
if ($term->slug == 'nz-made') {
  get_header('2');
}else {
  get_header();
}
?>
<section id="todaysDeal">
  <div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="title">
                <h3><?php echo $term->name; ?> <span>Deals</span></h3>
            </div>
        </div>
    </div>
<?php
$args = array('post_type' => 'Circular', 'meta_key'=>'circular_end_date', 'orderby' => 'meta_value_num', 'order' => 'ASC',
'tax_query' => array(
    array(
      'taxonomy' => 'product_category',
      'field'    => 'slug',
      'terms'    => array($term->slug),
    ),
  )
);
$loop = new WP_Query($args);
if ( $loop->have_posts() ) :
?>

    <div class="row">
        <div class="col-md-12">
            <ul class="dealsList">
              <?php
                  while ( $loop->have_posts() ) : $loop->the_post();
              ?>
              <?php get_template_part( 'content' );  ?>
            <?php endwhile; ?>
          </ul>
      </div>
    </div>
<?php endif; ?>
<?php wp_reset_query(); ?>
  </div>
</section>
<?php get_footer(); ?>
