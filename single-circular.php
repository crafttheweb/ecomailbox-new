<?php get_header(); ?>

<?php
$start_date = date('M d, Y', strtotime(get_post_meta($post->ID, "circular_start_date", true)));
$end_date = date('M d, Y', strtotime(get_post_meta($post->ID, "circular_end_date", true)));
 ?>
 <section id="todaysDeal">
   <div class="container">
     <div class="circulars">
       <div class="row">
           <div class="col-md-12">
               <div class="title text-center">
                   <h3>
                      <strong><?php the_title(); ?></strong>
                   </h3>
                   <p>
                     <strong>Valid: </strong><?php echo $start_date; ?> to <?php echo $end_date; ?>
                   </p>
               </div>
           </div>
       </div>

       <div class="row">
         <div class="col-md-8 col-md-push-2">
           <div class="circularWrap">
             <div class="circularLogoWrap">
               <div class="circularLogo text-center">
                 <a href="//<?php the_field('circular_url'); ?>">
                   <?php
                   $author = get_the_author_meta('ID');
                   $url = get_field('logo', 'user_'. $author );

                   if ($url != "") {
                   ?>
                   <img src="<?php echo $url; ?>">
                   <?php
                    }
                    ?>
                 </a>
               </div>
             </div>
             <div class="circularContent text-center">
               <a href="//<?php the_field('circular_url'); ?>">
                 <?php
                 remove_filter ('acf_the_content', 'wpautop');
                 the_field('circular_description');
                 ?>
               </a>
             </div>
           </div>
         </div>

       </div>
     </div>
   </div>
 </section>
<?php get_footer(); ?>
