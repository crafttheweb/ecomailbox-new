<form role="search" method="get" action="<?php echo home_url( '/' ); ?>">
  <div class="input-group">
    <div class="input-group-btn">
      <fieldset class="select_box">
        <select class="searchDrop form-control" name="location">
          <?php
          // $IPaddress  =   $_SERVER['REMOTE_ADDR'];
          // global $ipDetails;
          // $ipDetails = ip_details($IPaddress);

          // Get a list of all terms in a taxonomy
          $terms = get_terms( "location", array(
            'hide_empty' => 0,
          ) );
          $locations = array();
          if ( count($terms) > 0 ):
            foreach ( $terms as $term )
            $locations[] = $term->name;
            ?>
          <?php endif;?>

          <?php
          $selected = "";
          foreach($locations as $item){
            if($item == "South Island" || $item == "North Island") { unset($locations[$item]); } else {
              if (isset($_GET['location']) && $_GET['location'] == $item || $ipDetails->city == $item) {
                echo '<option value="'.$item.'" selected="'.$selected.'">'.$item.'</option>';
              }elseif ($getlocation == $item) {
                echo '<option value="'.$item.'" selected="'.$selected.'">'.$item.'</option>';
              }else {
                echo '<option value="'.$item.'">'.$item.'</option>';
              }
            }
          }
          ?>
        </select>
      </fieldset>
    </div>
    <input type="search" class="form-control" placeholder="Search..." value="<?php echo get_search_query(); ?>" name="s" title="search">
    <span class="input-group-btn">
      <button class="btn btn-search" type="submit"><span class="fa fa-search"></span></button>
    </span>
  </div>
</form>
