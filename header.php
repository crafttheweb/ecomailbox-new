<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Ecomailbox</title>
<?php wp_head(); ?>
</head>

<body>
    <nav class="navbar navbar-inverse">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <a class="navbar-brand" href="<?php echo home_url(); ?>"><img src="<?php bloginfo('template_directory'); ?>/images/logo.png"></a>

		   <!-- <form action="index.html" method="post" class="subscribe-form">
      <input type="email" name="email" class="subscribe-input" placeholder="Email address" autofocus>
      <button type="submit" class="subscribe-submit">Subscribe</button>
    </form> -->

        </div>
      </div>
    </nav>
    <header id="search">
        <?php
          $args = array('post_type' => 'Slider', 'orderby' => 'title', 'order' => 'ASC');
          $loop = new WP_Query($args);

        if (isset($loop)) :
          if ( $loop->have_posts() ) : ?>
          <ul class="bxslider">
          <?php
            while ( $loop->have_posts() ) : $loop->the_post();
          ?>

          <li><img src="<?php the_field('slider_image'); ?>" /></li>

          <?php

            endwhile;
          ?>
        </ul>

        <?php
          endif;
        endif;
          wp_reset_query();
        ?>


        <div class="searchPanel">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="text-center">See today's promotions &amp; deals from your favourite stores </h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8 col-md-push-2">
                      <?php get_search_form(); ?>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <section id="category">
        <nav class="navbar navbar-default navbar-left navbar-fixed-top categoriesNav">
          <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header categoryHeader">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mobileMenu" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <!--<div class="collapse navbar-collapse" id="categoriesNavi">-->

			<?php
			$args = array(

				'menu'	=> 'primary',
				'theme_location' => 'primary',
				'menu_class' => 'nav navbar-nav desktopMenu',
				'container'	=> 'div',
				'container_class' => 'collapse navbar-collapse',
				'container_id' => 'desktopMenu',
				'walker' => new walker_menu_Desktop()
				);
			wp_nav_menu($args);

      $args = array(

				'menu'	=> 'primary',
				'theme_location' => 'primary',
				'menu_class' => 'nav navbar-nav mobileMenu',
				'container'	=> 'div',
				'container_class' => 'collapse navbar-collapse',
				'container_id' => 'mobileMenu',
				'walker' => new walker_menu_Mobile()
				);
			wp_nav_menu($args);

			?>
          </div><!-- /.container-fluid -->
        </nav>

    </section>
