<?php
class walker_menu_Desktop extends Walker_Nav_Menu{
  function start_lvl(&$output, $depth = 0, $args = array()){
    $indent = str_repeat("\t", $depth);
    $submenu = ($depth) ? 'sub-menu' : '';
    $output .= "\n$indent<ul class=\"hoverDrop $submenu\">\n";
  }
  function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0){
    $indent = ($depth) ? str_repeat("\t", $depth) : '';
    $li_attributes = '';
    $class_names = $value = '';

    $classes = empty($item->classes) ? array() : (array) $item->classes;

    $classes[] = ($args->walker->has_children) ? 'primaryDrop' : '';
    $classes[] = ($item->current || $item->current_item_anchestor) ? 'active' : '';
    $classes[] = 'menu-item-' . $item->ID;

    if ($depth && $args->walker->has_children) {
      $classes[] = 'dropdown-submenu';
    }

    $class_names = join( ' ', apply_filters('nav_menu_css_class', array_filter( $classes ),$item, $args ) );
    $class_names = 'class="'. esc_attr($class_names) .'"';

    $id = apply_filters('nav_menu_item_id', 'menu-item-'.$item->ID, $item, $args);
    $id = strlen($id) ? ' id="'. esc_attr( $id ) .'"' : '';

    $output .= $indent . '<li ' . $id . $value . $class_names . $li_attributes . '>';

    $attributes = !empty( $item->attr_title ) ? ' title="'. esc_attr($item->attr_title) .'"' : '';
    $attributes .= !empty( $item->target ) ? ' target="'. esc_attr($item->target) .'"' : '';
    $attributes .= !empty( $item->xfn ) ? ' rel="'. esc_attr($item->xfn) .'"' : '';
    $attributes .= !empty( $item->url ) ? ' href="'. esc_attr($item->url) .'"' : '';

    // $attributes .= ( $args->walker->has_children ) ? ' class="dropdown-toggle" data-toggle="dropdown"' : '';

    $item_output = $args->before;
    $item_output .= '<a '. $attributes . '>';
    $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
    $item_output .= ($depth == 0 && $args->walker->has_children) ? '</a>' : '</a>';
    $item_output .= $args->after;

    $output .= apply_filters( 'Walker_Nav_Menu_start_el', $item_output, $item, $depth, $args );

  }

}

class walker_menu_Mobile extends Walker_Nav_Menu{
  function start_lvl(&$output, $depth = 0, $args = array()){
    $indent = str_repeat("\t", $depth);
    $submenu = ($depth) ? 'sub-menu' : '';
    $output .= "\n$indent<ul class=\"dropdown-menu $submenu\">\n";
  }
  function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0){
    $indent = ($depth) ? str_repeat("\t", $depth) : '';
    $li_attributes = '';
    $class_names = $value = '';

    $classes = empty($item->classes) ? array() : (array) $item->classes;

    $classes[] = ($args->walker->has_children) ? 'dropdown' : '';
    $classes[] = ($item->current || $item->current_item_anchestor) ? 'active' : '';
    $classes[] = 'menu-item-' . $item->ID;

    if ($depth && $args->walker->has_children) {
      $classes[] = 'dropdown-submenu';
    }

    $class_names = join( ' ', apply_filters('nav_menu_css_class', array_filter( $classes ),$item, $args ) );
    $class_names = 'class="'. esc_attr($class_names) .'"';

    $id = apply_filters('nav_menu_item_id', 'menu-item-'.$item->ID, $item, $args);
    $id = strlen($id) ? ' id="'. esc_attr( $id ) .'"' : '';

    $output .= $indent . '<li ' . $id . $value . $class_names . $li_attributes . '>';

    $attributes = !empty( $item->attr_title ) ? ' title="'. esc_attr($item->attr_title) .'"' : '';
    $attributes .= !empty( $item->target ) ? ' target="'. esc_attr($item->target) .'"' : '';
    $attributes .= !empty( $item->xfn ) ? ' rel="'. esc_attr($item->xfn) .'"' : '';
    $attributes .= !empty( $item->url ) ? ' href="'. esc_attr($item->url) .'"' : '';

    $attributes .= ( $args->walker->has_children ) ? ' class="dropdown-toggle" data-toggle="dropdown"' : '';

    $item_output = $args->before;
    $item_output .= '<a '. $attributes . '>';
    $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
    $item_output .= ($depth == 0 && $args->walker->has_children) ? '<span class="caret"></span></a>' : '</a>';
    $item_output .= $args->after;

    $output .= apply_filters( 'Walker_Nav_Menu_start_el', $item_output, $item, $depth, $args );

  }
}

add_action( 'after_setup_theme', 'bootstrap_setup' );
if ( ! function_exists( 'bootstrap_setup' ) ):

  function bootstrap_setup(){
  add_action( 'init', 'register_menu' );

  function register_menu(){
  register_nav_menus(array('primary' => __( 'Primary Menu'), 'primary2' => __( 'Primary Menu2'),

  'footer' => __( 'Footer Menu'),'footer2' => __( 'Footer Menu2'),'footer3' => __( 'Footer Menu3'),'footer4' => __( 'Footer Menu4'),

  ));
  }
}

endif;

?>
