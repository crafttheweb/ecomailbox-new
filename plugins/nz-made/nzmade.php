<?php
/*
Plugin Name: Ecomailbox NZ Made Plugin
Plugin URI: https://www.ecomailbox.com
Description: A plugin to manage the deals on the ecomailbox Website
Version: 1.1
Author: MIT Amazing Developers
Text Domain: NZ Made
*/

/*********************************
* GLobal Variables
*********************************/

//$emb_options = get_option('emb_circulars'); // retrieve plugin setting from the options table

/*********************************
* Includes
*********************************/
//include('includes/admin-page.php'); //Wordpress Admin page for adding Circulars HTML and save function...
namespace nz_made_plugin;

add_action('init', function(){

	$labels = array(
		'name'                  => _x( 'NZ Made', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'NZ Made', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'NZ Made', 'text_domain' ),
		'name_admin_bar'        => __( 'Post Type', 'text_domain' ),
		'archives'              => __( 'NZ Made Archives', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent NZ Made:', 'text_domain' ),
		'all_items'             => __( 'All NZ Made', 'text_domain' ),
		'add_new_item'          => __( 'Add New NZ Made', 'text_domain' ),
		'add_new'               => __( 'Add New NZ Made', 'text_domain' ),
		'new_item'              => __( 'New NZ Made', 'text_domain' ),
		'edit_item'             => __( 'Edit NZ Made', 'text_domain' ),
		'update_item'           => __( 'Update NZ Made', 'text_domain' ),
		'view_item'             => __( 'View NZ Made', 'text_domain' ),
		'search_items'          => __( 'Search NZ Made', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into NZ Made', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'nz_made_post', 'text_domain' ),
		'description'           => __( 'Post Type Description', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array('title','thumbnail','author'),
		'taxonomies'            => array(),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 2,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type('nz_made_post', $args );
});

add_action('init', function(){
	$labels = array(
		'name'                       => _x( 'NZ Made Product Category', 'taxonomy general name', 'text_domain' ),
		'singular_name'              => _x( 'NZ Made Product Category', 'taxonomy singular name', 'text_domain' ),
		'menu_name'                  => __( 'NZ Made Product Categories' ),
		'search_items'               => __( 'Search Product' ),
		'all_items'                  => __( 'All NZ Made Product Categories' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit NZ Made Product Categories' ),
		'update_item'                => __( 'Update NZ Made Product Categories' ),
		'add_new_item'               => __( 'Add New NZ Made Product Categories' ),
		'new_item_name'              => __( 'New NZ Made Product Categories Name' ),
		'add_or_remove_items'        => __( 'Add or remove NZ Made Product Categories' ),
		'not_found'                  => __( 'No NZ Made Product Categories found.' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'nz-made-product-category' ),
	);


	register_taxonomy('nz_made_product_category', 'nz_made_post', $args);
});

add_action('init', function(){

	$labels = array(
		'name'                       => _x( 'NZ Made Locations', 'taxonomy general name', 'text_domain' ),
		'singular_name'              => _x( 'NZ Made Location', 'taxonomy singular name', 'text_domain' ),
		'search_items'               => __( 'NZ Made Search Location' ),
		'popular_items'              => __( 'Popular NZ Made Location' ),
		'all_items'                  => __( 'All NZ Made Location' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit NZ MadeLocation' ),
		'update_item'                => __( 'Update NZ Made Location' ),
		'add_new_item'               => __( 'Add New NZ MadeLocation' ),
		'new_item_name'              => __( 'New NZ Made Location Name' ),
		'separate_items_with_commas' => __( 'Separate NZ Made Location with commas' ),
		'add_or_remove_items'        => __( 'Add or remove NZ Made Location' ),
		'choose_from_most_used'      => __( 'Choose from the most used NZ Made Locations' ),
		'not_found'                  => __( 'No NZ Made Location found.' ),
		'menu_name'                  => __( 'NZ Made Location' ),
	);
	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'nz-made-location' ),
	);

	register_taxonomy('nz_made_location', 'nz_made_post', $args);
});

add_action('init', function(){
	$labels = array(
		'name'                       => _x( 'NZ Made Home Section', 'taxonomy general name', 'text_domain' ),
		'singular_name'              => _x( 'NZ Made Home Section', 'taxonomy singular name', 'text_domain' ),
		'search_items'               => __( 'NZ Made Search Home' ),
		'all_items'                  => __( 'All NZ Made Home Sections' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit NZ Made Home Section' ),
		'update_item'                => __( 'Update NZ Made Home Section' ),
		'add_new_item'               => __( 'Add New NZ Made Home Section' ),
		'new_item_name'              => __( 'New NZ Made Home Section Name' ),
		'add_or_remove_items'        => __( 'Add or remove NZ Made Home Sections' ),
		'not_found'                  => __( 'No NZ Made Section found.' ),
		'menu_name'                  => __( 'NZ Made Home Sections' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'nz-made-home-section' ),
	);


	register_taxonomy('nz_made_home_page_section', 'nz_made_post', $args);
});

 ?>
