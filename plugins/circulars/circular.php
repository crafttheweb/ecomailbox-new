<?php
/*
Plugin Name: Ecomailbox Circular Plugins
Plugin URI: https://www.ecomailbox.com
Description: A plugin to manage the deals on the ecomailbox Website
Version: 1.0
Author: MIT Amazing Developers
Text Domain: Circulars
*/

/*********************************
* GLobal Variables
*********************************/

//$emb_options = get_option('emb_circulars'); // retrieve plugin setting from the options table

/*********************************
* Includes
*********************************/
//include('includes/admin-page.php'); //Wordpress Admin page for adding Circulars HTML and save function...
namespace ecomailbox_circular_plugin;

add_action('init', function(){

	$labels = array(
		'name'                  => _x( 'Circular', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Circular', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Circulars', 'text_domain' ),
		'name_admin_bar'        => __( 'Post Type', 'text_domain' ),
		'archives'              => __( 'Circular Archives', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Circular:', 'text_domain' ),
		'all_items'             => __( 'All Circulars', 'text_domain' ),
		'add_new_item'          => __( 'Add New Circular', 'text_domain' ),
		'add_new'               => __( 'Add New Circular', 'text_domain' ),
		'new_item'              => __( 'New Circular', 'text_domain' ),
		'edit_item'             => __( 'Edit Circular', 'text_domain' ),
		'update_item'           => __( 'Update Circular', 'text_domain' ),
		'view_item'             => __( 'View Circular', 'text_domain' ),
		'search_items'          => __( 'Search Circular', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into Circular', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'circular', 'text_domain' ),
		'description'           => __( 'Post Type Description', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array('title','thumbnail','author'),
		'taxonomies'            => array(),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 2,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'circular', $args );
});

add_action('init', function(){
	$labels = array(
		'name'                       => _x( 'Product Category', 'taxonomy general name', 'text_domain' ),
		'singular_name'              => _x( 'Product Category', 'taxonomy singular name', 'text_domain' ),
		'menu_name'                  => __( 'Product Categories' ),
		'search_items'               => __( 'Search Product' ),
		'all_items'                  => __( 'All Product Categories' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Product Categories' ),
		'update_item'                => __( 'Update Product Categories' ),
		'add_new_item'               => __( 'Add New Product Categories' ),
		'new_item_name'              => __( 'New Product Categories Name' ),
		'add_or_remove_items'        => __( 'Add or remove Product Categories' ),
		'not_found'                  => __( 'No Product Categories found.' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'product-category' ),
	);


	register_taxonomy('product_category', 'circular', $args);
});

add_action('init', function(){

	$labels = array(
		'name'                       => _x( 'Locations', 'taxonomy general name', 'text_domain' ),
		'singular_name'              => _x( 'Location', 'taxonomy singular name', 'text_domain' ),
		'search_items'               => __( 'Search Location' ),
		'popular_items'              => __( 'Popular Location' ),
		'all_items'                  => __( 'All Location' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Location' ),
		'update_item'                => __( 'Update Location' ),
		'add_new_item'               => __( 'Add New Location' ),
		'new_item_name'              => __( 'New Location Name' ),
		'separate_items_with_commas' => __( 'Separate Location with commas' ),
		'add_or_remove_items'        => __( 'Add or remove Location' ),
		'choose_from_most_used'      => __( 'Choose from the most used Locations' ),
		'not_found'                  => __( 'No Location found.' ),
		'menu_name'                  => __( 'Location' ),
	);
	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'location' ),
	);

	register_taxonomy('location', 'circular', $args);
});

add_action('init', function(){
	$labels = array(
		'name'                       => _x( 'Home Section', 'taxonomy general name', 'text_domain' ),
		'singular_name'              => _x( 'Home Section', 'taxonomy singular name', 'text_domain' ),
		'search_items'               => __( 'Search Home' ),
		'all_items'                  => __( 'All Home Sections' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Home Section' ),
		'update_item'                => __( 'Update Home Section' ),
		'add_new_item'               => __( 'Add New Home Section' ),
		'new_item_name'              => __( 'New Home Section Name' ),
		'add_or_remove_items'        => __( 'Add or remove Home Sections' ),
		'not_found'                  => __( 'No Section found.' ),
		'menu_name'                  => __( 'Home Sections' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'home-section' ),
	);


	register_taxonomy('home_page_section', 'circular', $args);
});

 ?>
