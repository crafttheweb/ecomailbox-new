<?php

function emb_options_page(){

  global $emb_options;

  ob_start(); ?>
  <div class="wrap">
    <h1>Circulars</h1>
    <p>You can add mailers from here</p>

    <form method="post" action="options.php">

      <?php settings_fields('emb_settings_group') ?>

      <h4><?php _e('Circular Information','emb_domain'); ?></h4>
      <p>
        <label class="description" for="emb_circulars[circular_url]"><?php _e('Circular URL:','emb_domain') ?></label>
        <input id="emb_circulars[circular_url]" name="emb_circulars[circular_url]" value="<?php echo $emb_options['circular_url'] ?>" />
      </p>
      <p>
        <label class="description" for="emb_circulars[circular_start_date]"><?php _e('Start Date:', 'emb_domain') ?></label>
        <input id="emb_circulars[circular_start_date]" name="emb_circulars[circular_start_date]" value="<?php echo $emb_options['circular_start_date'] ?>" />
      </p>
      <p>
        <label class="description" for="emb_circulars[circular_end_date]"><?php _e('End Date:', 'emb_domain') ?></label>
        <input id="emb_circulars[circular_end_date]" name="emb_circulars[circular_end_date]" value="<?php echo $emb_options['circular_end_date'] ?>" />
      </p>
      <p>
        <label class="description" for="emb_circulars[circular_content]"><?php _e('Description:', 'emb_domain') ?></label>
        <textarea id="emb_circulars[circular_content]" name="emb_circulars[circular_content]"><?php echo $emb_options['circular_content'] ?></textarea>
      </p>

      <p class="submit">
        <input type="submit" class="button-primary" value="<?php _e('Save', 'emb_domain') ?>" />
      </p>

    </form>
  </div>
<?php
echo ob_get_clean();
}

function emb_add_options_link(){
  //add_options_page('Title for the page','Name of the Plugin','Authorization','slug','function name');
  add_menu_page('Add Circulars','Circulars','manage_options','emb-circulars','emb_options_page','dashicons-admin-post','1');
}

add_action('admin_menu','emb_add_options_link');

function emb_register_settings(){
  //register_setting('Setting Group Name','Setting Name');
  register_setting('emb_settings_group','emb_circulars');
}
add_action('admin_init','emb_register_settings');
 ?>
