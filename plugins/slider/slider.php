<?php
/*
Plugin Name: Ecomailbox Slider Plugins
Plugin URI: https://www.ecomailbox.com
Description: A plugin to manage the slider images on the ecomailbox Website
Version: 1.0
Author: MIT Amazing Developers
Text Domain: Sliders
*/

//namespace ecomailboxSlider
namespace ecomailbox_slider_plugin;

add_action('init', function(){

	$labels = array(
		'name'                  => _x( 'Slider', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Slider', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Slider', 'text_domain' ),
		'name_admin_bar'        => __( 'Post Type', 'text_domain' ),
		'archives'              => __( 'Slider Archives', 'text_domain' ),
		'all_items'             => __( 'All Slider Images', 'text_domain' ),
		'add_new_item'          => __( 'Add New Slider Image', 'text_domain' ),
		'add_new'               => __( 'Add New Slider Image', 'text_domain' ),
		'new_item'              => __( 'New Slider Image', 'text_domain' ),
		'edit_item'             => __( 'Edit Slider Image', 'text_domain' ),
		'update_item'           => __( 'Update Slider Image', 'text_domain' ),
		'view_item'             => __( 'View Slider Image', 'text_domain' ),
		'search_items'          => __( 'Search Slider Images', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into Circular', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'slider', 'text_domain' ),
		'description'           => __( 'Post Type Slider', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array('title','featured_image'),
		'taxonomies'            => array(),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 3,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'Slider', $args );
});

add_action('init', function(){
	$labels = array(
		'name'                       => _x( 'Slider Page', 'taxonomy general name', 'text_domain' ),
		'singular_name'              => _x( 'Slider Page', 'taxonomy singular name', 'text_domain' ),
		'search_items'               => __( 'Search Slider' ),
		'all_items'                  => __( 'All Slider Pages' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Slider Page' ),
		'update_item'                => __( 'Update Slider Page' ),
		'add_new_item'               => __( 'Add New Slider Page' ),
		'new_item_name'              => __( 'New Slider Page' ),
		'add_or_remove_items'        => __( 'Add or remove Slider Pages' ),
		'not_found'                  => __( 'No Slider found.' ),
		'menu_name'                  => __( 'Slider Pages' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'slider-page' ),
	);


	register_taxonomy('slider_page', 'slider', $args);
});
 ?>
