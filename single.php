<?php get_header(); ?>

<section id="blog">
  <div class="container">


    <?php
    if (have_posts()) :
      while( have_posts() ): the_post();
    ?>
    <div class="row">
      <div class="col-md-12">
        <div class="title">
          <h3><?php the_title(); ?><span></span></h3>
        </div>
      </div>
    </div>
    <div class="row">
      <ul class="blogList">

        <li class="col-md-9 col-sm-9">
            <div class="blogContent">
              <div class="blogPic">
              <?php the_post_thumbnail();?>
              </div>
              <div class="blogDetails">
                <ul>
                <li><p><span class="fa fa-user"></span> <?php echo the_author_meta('display_name'); ?></p></li>
                <li><p><span class="fa fa-calendar "></span><?php the_date('d/m/Y'); ?></p></li>
                </ul>

                <div class="blogProfilePic">
                  <?php
                  $author = get_the_author_meta('ID');
                  $url = get_field('logo', 'user_'. $author );

                  if ($url != ""):
                  ?>
                  <img src="<?php echo $url; ?>">
                  <?php endif; ?>
                </div>
              </div>
              <div class="blogText">
                <?php
                the_content();
                ?>
              </div>
            </div>
        </li>
      </ul>
    </div>

  <?php
    endwhile;
  endif;
  ?>

  </div>
</section>

<?php get_footer(); ?>
