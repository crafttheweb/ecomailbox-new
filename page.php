<?php get_header(); ?>

<section id="todaysDeal">
    <div class="container">
          <div class="row">
              <div class="col-md-8 col-md-push-2">
                  <div class="title">
                      <h3><?php the_title(); ?><span></span></h3>
                  </div>
              </div>
          </div>
          <div class="row">
              <div class="col-md-8 col-md-push-2">
                <div class="textContent">
                  <?php the_content(); ?>
                </div>
              </div>
        </div>
		</div>
</section>
<?php get_footer(); ?>
