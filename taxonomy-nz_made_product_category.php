<?php get_header('2');
$term = get_term_by('slug', get_query_var('nz_made_product_category'), 'nz_made_product_category' );
?>
<section id="todaysDeal">
	<div class="container">
    <div class="row">
          <div class="col-md-12">
              <div class="title">
                  <h3><?php echo $term->name; ?> <span>Deals</span></h3>
              </div>
          </div>
      </div>
			<?php
			$args = array('post_type' => 'nz_made_post', 'meta_key'=>'circular_end_date', 'orderby' => 'meta_value_num', 'order' => 'ASC',
			'tax_query' => array(
			    array(
			      'taxonomy' => 'nz_made_product_category',
			      'field'    => 'slug',
			      'terms'    => array($term->slug),
			    ),
			  )
			);
			$loop = new WP_Query($args);
			if ( $loop->have_posts() ) :
			?>
      <div class="row">
          <div class="col-md-12">
              <ul class="dealsList">
                <?php
                  while ( $loop->have_posts() ) : $loop->the_post();
                ?>
                <?php
									get_template_part( 'content' );
								?>
              <?php endwhile; ?>
            </ul>
        </div>
      </div>
			<?php endif; ?>
			<?php wp_reset_query(); ?>
	</div>
</section>

<?php get_footer(); ?>
