<?php get_header();
$dealsSection = '';

if (isset($_GET['viewAll'])) {
  $dealsSection = $_GET['viewAll'];
  $args = array('post_type' => 'Circular', 'meta_key'=>'position', 'orderby' => 'meta_value_num', 'order' => 'ASC',
  'tax_query' => array(
      array(
        'taxonomy' => 'home_page_section',
        'field'    => 'slug',
        'terms'    => array($dealsSection),
      ),
    )
  );

  if ($dealsSection == 'todays-deals') {
    $loop = new WP_Query($args);
    if ( $loop->have_posts() ) :
  ?>
  <section id="todaysDeal">
      <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title">
                    <h3>Todays <span>Deals</span></h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <ul class="dealsList">
                  <?php
                    	while ( $loop->have_posts() ) : $loop->the_post();
                  ?>
                  <?php get_template_part( 'content' );  ?>
                <?php endwhile; ?>
              </ul>
          </div>
        </div>
  		</div>
  </section>
  <?php endif; ?>
  <?php
    wp_reset_query();
  }

  if ($dealsSection == 'this-weeks-mailers') {
    $loop = new WP_Query($args);
    if ( $loop->have_posts() ) :
  ?>
  <section id="todaysDeal">
      <div class="container">
        <div class="row">
            <div class="col-md-12">
              <div class="title">
                  <h3>This Week's <span>Mailers</span></h3>
              </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <ul class="dealsList">
                  <?php
                    	while ( $loop->have_posts() ) : $loop->the_post();
                  ?>
                  <?php get_template_part( 'content' );  ?>
                <?php endwhile; ?>
              </ul>
          </div>
        </div>
      </div>
  </section>
  <?php endif; ?>
  <?php
    wp_reset_query();
  }

  if ($dealsSection == 'more-deals') {
    $loop = new WP_Query($args);
    if ( $loop->have_posts() ) :
  ?>
  <section id="todaysDeal">
      <div class="container">
        <div class="row">
            <div class="col-md-12">
              <div class="title">
                  <h3>More <span>Deals</span></h3>
              </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <ul class="dealsList">
                  <?php
                    	while ( $loop->have_posts() ) : $loop->the_post();
                  ?>
                  <?php get_template_part( 'content' );  ?>
                <?php endwhile; ?>
              </ul>
          </div>
        </div>
      </div>
  </section>
  <?php endif; ?>
  <?php
    wp_reset_query();
  }

  if ($dealsSection == 'blog') { ?>
    <section id="blog">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="title">
              <h3>Our <span>Blogs</span></h3>
            </div>
          </div>
        </div>

        <?php
        $args = array( 'numberposts' => 3);
        $posts= get_posts( $args );
        if ($posts) :
        ?>
        <div class="row">
          <ul class="blogList">
            <?php get_template_part( 'content-all-blog' );  ?>
          </ul>
        </div>
      <?php endif; ?>

      </div>
    </section>
  <?php
  }
}
?>

<?php get_footer(); ?>
