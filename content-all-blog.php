<?php foreach ( $posts as $post ) :
setup_postdata($post); ?>

<li class="col-md-4 col-sm-6">
  <a href="<?php echo esc_url( get_permalink($post->ID) ); ?>">
    <div class="blogContent">
      <div class="blogPic">
      <?php the_post_thumbnail();?>
      </div>
      <div class="blogDetails">
        <ul>
        <li><p><span class="fa fa-user"></span> <?php echo the_author_meta('display_name'); ?></p></li>
        <li><p><span class="fa fa-calendar "></span><?php the_date('d/m/Y'); ?></p></li>
        </ul>

        <div class="blogProfilePic">
          <?php
          $author = get_the_author_meta('ID');
          $url = get_field('logo', 'user_'. $author );

          if ($url != ""):
          ?>
          <img src="<?php echo $url; ?>">
          <?php endif; ?>
        </div>
      </div>
      <div class="blogText">
        <?php
        $excerpt = get_the_excerpt();
        echo string_limit_words($excerpt,30).'...<strong>Read more</strong>';
        ?>
      </div>
    </div>
  </a>
</li>
<?php endforeach; ?>
